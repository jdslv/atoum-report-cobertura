<?php

$runner
    ->addExtension(new atoum\atoum\xml\extension($script))
    ->addTestsFromDirectory(__DIR__ . '/tests/units')
;

// Show default report
$script->addDefaultReport();

// reports
if (extension_loaded('xdebug') === true) {
    $script->noCodeCoverageInDirectories(__DIR__ . '/vendor');

    $coverage = new atoum\atoum\reports\coverage\html();
    $coverage
        ->addWriter(new atoum\atoum\writers\std\out())
        ->setOutPutDirectory('./reports')
    ;
    $runner->addReport($coverage);

    if (getenv('CI')) {
        // coverage report
        $cobertura = new atoum\atoum\reports\cobertura();
        $cobertura->addWriter(new atoum\atoum\writers\file('reports/cobertura.xml'));
        $runner->addReport($cobertura);

        // xunit report
        $xunit = new atoum\atoum\reports\sonar\xunit();
        $xunit->addWriter(new atoum\atoum\writers\file('reports/junit.xml'));
        $runner->addReport($xunit);
    }
}
