<?php

declare(strict_types=1);

namespace atoum\atoum\reports\cobertura\tests\units;

use atoum\atoum\reports\cobertura;
use DateTime;
use ReflectionNamedType;
use ReflectionType;
use stdClass;

use const PHP_INT_MAX;

trait provider
{
    public function baseClass()
    {
        $class = new class() {
            public function method() {}
        };

        yield [$class, 'method()'];

        $class = new class() {
            public function method($a) {}
        };

        yield [$class, 'method($a)'];

        $class = new class() {
            public function method($a, $b) {}
        };

        yield [$class, 'method($a, $b)'];

        $class = new class() {
            public function method($a, ...$b) {}
        };

        yield [$class, 'method($a, ...$b)'];

        $class = new class() {
            public function method(string $a, int $b) {}
        };

        yield [$class, 'method(string $a, int $b)'];

        $class = new class() {
            public function method(string $a = '', int $b = 0, bool $c = false, bool $d = true) {}
        };

        yield [$class, 'method(string $a = \'\', int $b = 0, bool $c = false, bool $d = true)'];

        $class = new class() {
            public function method(array $a = [], $b = null) {}
        };

        yield [$class, 'method(array $a = [], $b = null)'];

        $class = new class() {
            public function method(&$a, &$b) {}
        };

        yield [$class, 'method(&$a, &$b)'];

        $class = new class() {
            public function method(): string
            {
                return '';
            }
        };

        yield [$class, 'method(): string'];

        $class = new class() {
            public function method(self $a): self
            {
                return $a;
            }
        };

        yield [$class, 'method(self $a): self'];

        $class = new class() {
            public function method(stdClass $a): string
            {
                return (string) $a;
            }
        };

        yield [$class, 'method(stdClass $a): string'];

        $class = new class() {
            public function method(cobertura\reflection\klass $a): ?DateTime
            {
                return new DateTime();
            }
        };

        yield [$class, 'method(atoum\atoum\reports\cobertura\reflection\klass $a): ?DateTime'];

        $class = new class() {
            public function method(cobertura\reflection\klass ...$a): cobertura\reflection\klass
            {
                return $a[0];
            }
        };

        yield [
            $class,
            sprintf('method(%s ...$a): %s', cobertura\reflection\klass::class, cobertura\reflection\klass::class),
        ];

        $class = new class() {
            public function method(&...$a) {}
        };

        yield [$class, 'method(&...$a)'];

        $class = new class() {
            public function method($a = PHP_INT_MAX) {}
        };

        yield [$class, 'method($a = PHP_INT_MAX)'];

        $class = new class() {
            public function method(callable $name) {}
        };

        yield [$class, 'method(callable $name)'];

        $class = new class() {
            public function method(float|int $a) {}
        };

        yield [$class, 'method(float|int $a)'];

        $class = new class() {
            public function method(ReflectionNamedType|ReflectionType $a): static
            {
                return $this;
            }
        };

        yield [$class, 'method(ReflectionNamedType|ReflectionType $a): static'];

        $class = new class() {
            public function method(?string $base = null): false|string
            {
                return false;
            }
        };

        yield [$class, 'method(?string $base = null): false|string'];
    }

    public function branchesData()
    {
        $branches = [];
        $branchesCovered = $branchesTotal = 0;
        $opsCovered = $opsTotal = 0;

        $ops = [];

        for ($idx = 0; $idx < 10; $idx++) {
            $randOp = random_int(1, 5);
            $hit = random_int(0, 1);

            $branches[$opsTotal] = [
                'op_start' => $opsTotal,
                'op_end' => $opsTotal + $randOp,
                'line_start' => 0,
                'line_end' => 0,
                'hit' => $hit,
                'out' => [],
                'out_hit' => [],
            ];

            for ($i = 0; $i <= $randOp; $i++) {
                $branches[$opsTotal]['out'][] = random_int(1, 9);
                $branches[$opsTotal]['out_hit'][] = $h = random_int(0, 1);

                $branchesCovered += $h;
                $branchesTotal++;
            }

            if ($hit) {
                $ops = array_unique(array_merge($ops, range($opsTotal, $opsTotal + $randOp)));
            }

            $opsTotal += $randOp + 1;
            $opsCovered = count($ops);
        }

        return [
            $branches,
            $branchesCovered,
            $branchesTotal,
            $opsCovered,
            $opsTotal,
        ];
    }

    public function linesData()
    {
        $lines = [];
        $filteredLines = [];
        $covered = $total = 0;

        for ($idx = 0; $idx < 20; $idx++) {
            $rand = random_int(0, 2);
            $item = 1;

            if ($rand) {
                $item = $rand * -1;
            }

            if ($item > -2) {
                $total++;
                $filteredLines[$idx] = (int) $item > 0;
            }

            if ($item > 0) {
                $covered++;
            }

            $lines[] = $item;
        }

        return [
            $lines,
            $covered,
            $total,
            $filteredLines,
        ];
    }

    public function pathsData()
    {
        $paths = [];
        $covered = $total = 0;

        for ($idx = 0; $idx < 10; $idx++) {
            $rand = random_int(1, 5);
            $hit = random_int(0, 1);

            $total++;
            $covered += $hit;

            $paths[] = [
                'path' => range(0, $rand),
                'hit' => $hit,
            ];
        }

        return [
            $paths,
            $covered,
            $total,
        ];
    }
}
