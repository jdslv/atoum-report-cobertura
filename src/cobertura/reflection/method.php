<?php

declare(strict_types=1);

namespace atoum\atoum\reports\cobertura\reflection;

use ReflectionMethod;
use ReflectionType;
use ReflectionUnionType;

class method extends ReflectionMethod
{
    protected object|string $currentClass;

    public function __construct(object|string $class, string $name)
    {
        parent::__construct($class, $name);

        $this->currentClass = $class;
    }

    public function getCurrentClass(): klass
    {
        return new klass($this->currentClass);
    }

    public function getFullName(): string
    {
        return $this->getCurrentClass()->getName() . '::' . $this->getName();
    }

    public function getShortSignature(): string
    {
        return vsprintf('%s(%s)%s', [
            $this->getShortName(),
            $this->getSignatureParameters(),
            $this->getSignatureReturnType(),
        ]);
    }

    public function getSignature(): string
    {
        return vsprintf('%s::%s(%s)%s', [
            $this->getCurrentClass()->getName(),
            $this->getName(),
            $this->getSignatureParameters(),
            $this->getSignatureReturnType(),
        ]);
    }

    protected function getSignatureParameters(): string
    {
        $parameters = [];

        foreach ($this->getParameters() as $parameter) {
            $parts = [];
            $name = '';

            $type = $parameter->getType();
            $types = $type instanceof ReflectionUnionType ? $type->getTypes() : [$type];

            if ($parameter->isPassedByReference()) {
                $name .= '&';
            }

            if ($parameter->isVariadic()) {
                $name .= '...';
            }

            $name .= '$' . $parameter->getName();

            if ($parameter->hasType()) {
                $tmp = [];

                foreach ($types as $type) {
                    $tmp[] = (string) $type;
                }

                sort($tmp);

                $parts[] = implode('|', $tmp);
            }

            $parts[] = $name;

            if ($parameter->isOptional() && !$parameter->isVariadic()) {
                $parts[] = '=';
                $default = $parameter->getDefaultValue();

                if ($parameter->isDefaultValueConstant()) {
                    $parts[] = $parameter->getDefaultValueConstantName();
                } elseif ($parameter->allowsNull()) {
                    $parts[] = 'null';
                } elseif (in_array('array', array_map(fn (ReflectionType $t) => (string) $t, $types), true)) {
                    $parts[] = '[]';
                } elseif (is_bool($default)) {
                    $parts[] = $default ? 'true' : 'false';
                } elseif (is_int($default) || is_float($default)) {
                    $parts[] = $default;
                } else {
                    $parts[] = sprintf("'%s'", $default);
                }
            }

            $parameters[] = implode(' ', $parts);
        }

        return implode(', ', $parameters);
    }

    protected function getSignatureReturnType(): string
    {
        if (!$this->hasReturnType()) {
            return '';
        }

        $types = explode('|', (string) $this->getReturnType());

        sort($types);

        return ': ' . join('|', array_map('trim', $types));
    }
}
