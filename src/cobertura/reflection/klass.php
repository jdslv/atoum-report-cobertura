<?php

declare(strict_types=1);

namespace atoum\atoum\reports\cobertura\reflection;

use ReflectionClass;

class klass extends ReflectionClass
{
    public function getFileName(?string $base = null): false|string
    {
        $path = parent::getFileName();

        if ($base) {
            $path = str_replace(rtrim($base, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR, '', $path);
        }

        return $path;
    }
}
