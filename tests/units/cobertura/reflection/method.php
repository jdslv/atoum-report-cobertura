<?php

declare(strict_types=1);

namespace atoum\atoum\reports\cobertura\tests\units\reflection;

use atoum;
use atoum\atoum\reports\cobertura;
use atoum\atoum\reports\cobertura\tests\units\provider;
use DateTime;
use ReflectionMethod;

class method extends atoum\atoum\test
{
    use provider;

    public function testClass()
    {
        $this
            ->testedClass
                ->extends(ReflectionMethod::class)
        ;
    }

    public function testGetCurrentClass()
    {
        $this
            ->assert('Test with object')
                ->if($this->newTestedInstance(new DateTime('now'), '__construct'))
                ->then
                    ->object($this->testedInstance->getCurrentClass())
                        ->isInstanceOf(cobertura\reflection\klass::class)

                    ->string($this->testedInstance->getCurrentClass()->getName())
                        ->isIdenticalTo(DateTime::class)

            ->assert('Test with string')
                ->if($this->newTestedInstance(DateTime::class, '__construct'))
                ->then
                    ->object($this->testedInstance->getCurrentClass())
                        ->isInstanceOf(cobertura\reflection\klass::class)

                    ->string($this->testedInstance->getCurrentClass()->getName())
                        ->isIdenticalTo(DateTime::class)
        ;
    }

    public function testGetFullName()
    {
        $this
            ->assert('Test with object')
                ->if($this->newTestedInstance(new DateTime('now'), '__construct'))
                ->then
                    ->string($this->testedInstance->getFullName())
                        ->isIdenticalTo('DateTime::__construct')

            ->assert('Test with string')
                ->if($this->newTestedInstance(DateTime::class, '__construct'))
                ->then
                    ->string($this->testedInstance->getFullName())
                        ->isIdenticalTo('DateTime::__construct')
        ;
    }

    /**
     * @dataProvider baseClass
     */
    public function testGetShortSignature(object $class, string $sign)
    {
        $this
            ->assert($sign)
                ->string($this->newTestedInstance($class, 'method')->getShortSignature())
                    ->isIdenticalTo($sign)
        ;
    }

    /**
     * @dataProvider baseClass
     */
    public function testGetSignature(object $class, string $sign)
    {
        $this
            ->assert($sign)
                ->string($this->newTestedInstance($class, 'method')->getSignature())
                    ->startWith('class@anonymous')
                    ->endWith('::' . $sign)
        ;
    }
}
