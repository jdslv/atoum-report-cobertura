<?php

declare(strict_types=1);

namespace atoum\atoum\reports\cobertura\tests\units;

use atoum;
use mock;

class extension extends atoum\atoum\test
{
    public function test__construct()
    {
        $this
            ->given($runner = new mock\atoum\atoum\runner)
            ->and($this->calling($runner)->addTestsFromDirectory->isFluent)
            ->and($parser = new mock\atoum\atoum\script\arguments\parser)

            ->if($script = new atoum\atoum\scripts\runner(uniqid()))
            ->and($script->setArgumentsParser($parser))
            ->and($script->setRunner($runner))

            ->if($configurator = new mock\atoum\atoum\configurator($script))
            ->then
                ->object($this->newTestedInstance)

            ->if($this->resetMock($parser))
            ->then
                ->object($this->newTestedInstance($configurator))
                ->mock($parser)
                    ->call('addHandler')->twice

                ->assert('Test with "--test-ext"')
                    ->object($parser->triggerHandlers('--test-ext', [], $script))
                        ->isIdenticalTo($parser)

                    ->mock($runner)
                        ->call('addTestsFromDirectory')
                            ->withArguments(realpath(__DIR__ . '/../../units'))->once

                ->assert('Test with "--test-it"')
                    ->object($parser->triggerHandlers('--test-it', [], $script))
                        ->isIdenticalTo($parser)

                    ->mock($runner)
                        ->call('addTestsFromDirectory')
                            ->withArguments(realpath(__DIR__ . '/../../units'))->once
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->extends(atoum\atoum\reports\extension::class)
        ;
    }
}
