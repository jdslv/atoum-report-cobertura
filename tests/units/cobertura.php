<?php

declare(strict_types=1);

namespace atoum\atoum\reports\tests\units;

use atoum;

class cobertura extends atoum\atoum\test
{
    public function testClass()
    {
        $this
            ->testedClass
                ->extends(atoum\atoum\reports\coverage::class)
        ;
    }

    public function testUniqueKeys()
    {
        $this
            ->if($key1 = uniqid())
            ->and($key2 = uniqid())
            ->then
                ->array($this->newTestedInstance->uniqueKeys())
                    ->isEmpty

                ->array($this->newTestedInstance->uniqueKeys([]))
                    ->isEmpty

                ->array($this->newTestedInstance->uniqueKeys([$key1 => uniqid()], [$key2 => uniqid()]))
                    ->hasSize(2)
                    ->contains($key1)
                    ->contains($key2)

                ->array($this->newTestedInstance->uniqueKeys([$key1 => uniqid(), $key2 => uniqid()], [$key1 => uniqid()]))
                    ->hasSize(2)
                    ->contains($key1)
                    ->contains($key2)
        ;
    }
}
