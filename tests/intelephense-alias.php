<?php

namespace mock\atoum\atoum;

class configurator extends \atoum\atoum\configurator {}
class runner extends \atoum\atoum\runner {}

namespace mock\atoum\atoum\script\arguments;

class parser extends \atoum\atoum\script\arguments\parser {}

namespace mock\atoum\atoum\reports\cobertura\reflection;

class method extends \atoum\atoum\reports\cobertura\reflection\method {}
